@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Profissão
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($profession, ['route' => ['professions.update', $profession->id], 'method' => 'patch']) !!}

                        @include('professions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
