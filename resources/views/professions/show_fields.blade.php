<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $profession->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $profession->name !!}</p>
</div>

<!-- Dweller Id Field -->
<div class="form-group">
    {!! Form::label('dweller_id', 'Morador:') !!}
    <p>{!! $profession->dwellers->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! date_format($profession->created_at,'d/m/Y H:i') !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! date_format($profession->updated_at,'d/m/Y H:i') !!}</p>
</div>
