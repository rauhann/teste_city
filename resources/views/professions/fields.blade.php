<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Dweller Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dweller_id', 'Morador:') !!}
    {!! Form::select('dweller_id', ['' => 'Selecione...'] + $dwellers, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('professions.index') !!}" class="btn btn-default">Cancelar</a>
</div>
