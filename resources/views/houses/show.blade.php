@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Casa
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('houses.show_fields')
                    <a href="{!! route('houses.index') !!}" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
