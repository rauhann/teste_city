<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $house->id !!}</p>
</div>

<!-- Street Field -->
<div class="form-group">
    {!! Form::label('street', 'Rua:') !!}
    <p>{!! $house->street !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Numero:') !!}
    <p>{!! $house->number !!}</p>
</div>

<!-- Neighborhood Id Field -->
<div class="form-group">
    {!! Form::label('neighborhood_id', 'Bairro:') !!}
    <p>{!! $house->neighborhood->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! date_format($house->created_at,'d/m/Y H:i') !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! date_format($house->updated_at,'d/m/Y H:i') !!}</p>
</div>

