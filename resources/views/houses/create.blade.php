@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Casa
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'houses.store']) !!}

                        @include('houses.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.select2').select2();
        });
    </script>
@endsection
