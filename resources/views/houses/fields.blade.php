<!-- Street Field -->
<div class="form-group col-sm-6">
    {!! Form::label('street', 'Rua:') !!}
    {!! Form::text('street', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-2">
    {!! Form::label('number', 'Numero:') !!}
    {!! Form::number('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Neighborhood Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('neighborhood_id', 'Bairro:') !!}
    {!! Form::select('neighborhood_id', ['' => 'Selecione...'] + $neighborhoods, null,['class' => 'form-control select2']) !!}
</div>

<div id="dwellers" class="form-group col-sm-12">
    <h4 style="margin: 40px 0 20px 0">
        Moradores:
    </h4>

    <div class="row">
        <div class="form-group col-sm-9">
            {!! Form::label('name', 'Nome:') !!}
            {!! Form::text('dwellers[name][]', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-2">
            {!! Form::label('age', 'Idade:') !!}
            {!! Form::number('dwellers[age][]', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-1 btnPlus" style="margin-top: 24px">
            {!! Form::button('+', ['class' => 'btn btn-success']) !!}
        </div>
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('houses.index') !!}" class="btn btn-default">Cancelar</a>
</div>

@section('scripts')

<script>
    function addDweller()
    {
        $('#dwellers').append(`<div class="row">
            <div class="form-group col-sm-9">
                {!! Form::label('name', 'Nome:') !!}
                {!! Form::text('dwellers[name][]', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('age', 'Idade:') !!}
                {!! Form::number('dwellers[age][]', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group btnPlus col-sm-1" style="margin-top: 24px">
                {!! Form::button('+', ['class' => 'btn btn-success']) !!}
            </div>
        </div>`);
    }

    $('#dwellers').on('click','.btnPlus', addDweller);
</script>

@endsection

