@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Morador
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'dwellers.store']) !!}

                        @include('dwellers.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.select2').select2();
        });
    </script>
@endsection
