<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $dweller->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $dweller->name !!}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Idade:') !!}
    <p>{!! $dweller->age !!}</p>
</div>

<!-- House Id Field -->
<div class="form-group">
    {!! Form::label('house_id', 'Endereco:') !!}
    <p>{!! $dweller->house->street !!}, {!! $dweller->house->number !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! date_format($dweller->created_at,'d/m/Y H:i') !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! date_format($dweller->updated_at,'d/m/Y H:i') !!}</p>
</div>

