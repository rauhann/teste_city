<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-1">
    {!! Form::label('age', 'Idade:') !!}
    {!! Form::number('age', null, ['class' => 'form-control']) !!}
</div>

<!-- House Id Field -->
<div class="form-group col-sm-5">
    {!! Form::label('house_id', 'Endereco:') !!}
    {!! Form::select('house_id', ['' => 'Selecione...'] + $houses, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('dwellers.index') !!}" class="btn btn-default">Cancelar</a>
</div>
