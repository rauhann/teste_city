@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Morador
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dweller, ['route' => ['dwellers.update', $dweller->id], 'method' => 'patch']) !!}

                        @include('dwellers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
