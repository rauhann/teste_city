@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row p-3">

        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="container2" style="min-width: 310px; height: 400px; margin: 20px auto"></div>

    </div>

</div>
@endsection

@section('scripts')

<script>

    var data = [];
    var data2 = [];

    var casasBairro = {!! $casasBairro !!}

    $.each(casasBairro,function(i,v)
    {
        data.push({name : i,data: [v]});
        data2.push({name : i,y: v});
    });

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: "Numero de casas por bairro"
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Quantidade'
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Total',
            data: data2,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Distribuicao percentual de casas por bairro'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percentual',
            colorByPoint: true,
            data: data2
        }]
    });

</script>

@endsection
