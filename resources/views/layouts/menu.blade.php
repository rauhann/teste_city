<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}"><i class="fa fa-circle"></i><span>Home</span></a>
</li>

<li class="{{ Request::is('neighborhoods*') ? 'active' : '' }}">
    <a href="{!! route('neighborhoods.index') !!}"><i class="fa fa-plus"></i><span>Bairros</span></a>
</li>

<li class="{{ Request::is('houses*') ? 'active' : '' }}">
    <a href="{!! route('houses.index') !!}"><i class="fa fa-plus"></i><span>Casas</span></a>
</li>

<li class="{{ Request::is('dwellers*') ? 'active' : '' }}">
    <a href="{!! route('dwellers.index') !!}"><i class="fa fa-plus"></i><span>Moradores</span></a>
</li>

<li class="{{ Request::is('professions*') ? 'active' : '' }}">
    <a href="{!! route('professions.index') !!}"><i class="fa fa-plus"></i><span>Profissões</span></a>
</li>
