<?php

namespace App\Http\Controllers;

use App\Models\House;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = House::with('neighborhood')->get()->groupBy('neighborhood_id');

        $i = 0;
        foreach($houses as $id => $house)
        {
            $data[$house[$i]->neighborhood->name] = count($house);
        }

        $casasBairro = json_encode($data);

        return view('home',compact('casasBairro'));
    }
}
