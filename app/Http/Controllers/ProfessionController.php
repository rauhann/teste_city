<?php

namespace App\Http\Controllers;

use App\DataTables\ProfessionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProfessionRequest;
use App\Http\Requests\UpdateProfessionRequest;
use App\Models\Dweller;
use App\Models\House;
use App\Models\Neighborhood;
use App\Repositories\ProfessionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProfessionController extends AppBaseController
{
    /** @var  ProfessionRepository */
    private $professionRepository;

    public function __construct(ProfessionRepository $professionRepo)
    {
        $this->professionRepository = $professionRepo;
    }

    /**
     * Display a listing of the Profession.
     *
     * @param ProfessionDataTable $professionDataTable
     * @return Response
     */
    public function index(ProfessionDataTable $professionDataTable)
    {
        return $professionDataTable->render('professions.index');
    }

    /**
     * Show the form for creating a new Profession.
     *
     * @return Response
     */
    public function create()
    {
        $dwellers = Dweller::orderBy('name')->pluck('name','id')->all();

        return view('professions.create',compact('dwellers'));
    }

    /**
     * Store a newly created Profession in storage.
     *
     * @param CreateProfessionRequest $request
     *
     * @return Response
     */
    public function store(CreateProfessionRequest $request)
    {
        $input = $request->all();

        $profession = $this->professionRepository->create($input);

        Flash::success('Profession saved successfully.');

        return redirect(route('professions.index'));
    }

    /**
     * Display the specified Profession.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profession = $this->professionRepository->find($id);

        if (empty($profession)) {
            Flash::error('Profession not found');

            return redirect(route('professions.index'));
        }

        return view('professions.show')->with('profession', $profession);
    }

    /**
     * Show the form for editing the specified Profession.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profession = $this->professionRepository->find($id);

        $dwellers = Dweller::orderBy('name')->pluck('name','id')->all();

        if (empty($profession)) {
            Flash::error('Profession not found');

            return redirect(route('professions.index'));
        }

        return view('professions.edit',compact('dwellers'))->with('profession', $profession);
    }

    /**
     * Update the specified Profession in storage.
     *
     * @param  int              $id
     * @param UpdateProfessionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfessionRequest $request)
    {
        $profession = $this->professionRepository->find($id);

        if (empty($profession)) {
            Flash::error('Profession not found');

            return redirect(route('professions.index'));
        }

        $profession = $this->professionRepository->update($request->all(), $id);

        Flash::success('Profession updated successfully.');

        return redirect(route('professions.index'));
    }

    /**
     * Remove the specified Profession from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profession = $this->professionRepository->find($id);

        if (empty($profession)) {
            Flash::error('Profession not found');

            return redirect(route('professions.index'));
        }

        $this->professionRepository->delete($id);

        Flash::success('Profession deleted successfully.');

        return redirect(route('professions.index'));
    }
}
