<?php

namespace App\Http\Controllers;

use App\DataTables\HouseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHouseRequest;
use App\Http\Requests\UpdateHouseRequest;
use App\Models\Neighborhood;
use App\Repositories\DwellerRepository;
use App\Repositories\HouseRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class HouseController extends AppBaseController
{
    /** @var  HouseRepository */
    private $houseRepository;
    private $dwellerRepository;

    public function __construct(HouseRepository $houseRepo, DwellerRepository $dwellerRepo)
    {
        $this->houseRepository   = $houseRepo;
        $this->dwellerRepository = $dwellerRepo;
    }

    /**
     * Display a listing of the House.
     *
     * @param HouseDataTable $houseDataTable
     * @return Response
     */
    public function index(HouseDataTable $houseDataTable)
    {
        return $houseDataTable->render('houses.index');
    }

    /**
     * Show the form for creating a new House.
     *
     * @return Response
     */
    public function create()
    {
        $neighborhoods = Neighborhood::orderBy('name')->pluck('name','id')->all();

        return view('houses.create',compact('neighborhoods'));
    }

    /**
     * Store a newly created House in storage.
     *
     * @param CreateHouseRequest $request
     *
     * @return Response
     */
    public function store(CreateHouseRequest $request)
    {
        $input = $request->all();

        $dwellers = $input['dwellers'];
        unset($input['dwellers']);

        $house = $this->houseRepository->create($input);

        $dwellersCount = count($dwellers['name']);

        for($i = 0;$i < $dwellersCount;$i++)
        {
            $this->dwellerRepository->create([
                'name' => $dwellers['name'][$i],
                'age' => $dwellers['age'][$i],
                'house_id' => $house->id
            ]);
        }

        Flash::success('House saved successfully.');

        return redirect(route('houses.index'));
    }

    /**
     * Display the specified House.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $house = $this->houseRepository->find($id);

        if (empty($house)) {
            Flash::error('House not found');

            return redirect(route('houses.index'));
        }

        return view('houses.show')->with('house', $house);
    }

    /**
     * Show the form for editing the specified House.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $house = $this->houseRepository->find($id);

        $neighborhoods = Neighborhood::orderBy('name')->pluck('name','id')->all();

        if (empty($house)) {
            Flash::error('House not found');

            return redirect(route('houses.index'));
        }

        return view('houses.edit',compact('neighborhoods'))->with('house', $house);
    }

    /**
     * Update the specified House in storage.
     *
     * @param  int              $id
     * @param UpdateHouseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHouseRequest $request)
    {
        $house = $this->houseRepository->find($id);

        if (empty($house)) {
            Flash::error('House not found');

            return redirect(route('houses.index'));
        }

        $house = $this->houseRepository->update($request->all(), $id);

        Flash::success('House updated successfully.');

        return redirect(route('houses.index'));
    }

    /**
     * Remove the specified House from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $house = $this->houseRepository->find($id);

        if (empty($house)) {
            Flash::error('House not found');

            return redirect(route('houses.index'));
        }

        $this->houseRepository->delete($id);

        Flash::success('House deleted successfully.');

        return redirect(route('houses.index'));
    }
}
