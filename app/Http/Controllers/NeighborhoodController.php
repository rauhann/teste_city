<?php

namespace App\Http\Controllers;

use App\DataTables\NeighborhoodDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNeighborhoodRequest;
use App\Http\Requests\UpdateNeighborhoodRequest;
use App\Repositories\NeighborhoodRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class NeighborhoodController extends AppBaseController
{
    /** @var  NeighborhoodRepository */
    private $neighborhoodRepository;

    public function __construct(NeighborhoodRepository $neighborhoodRepo)
    {
        $this->neighborhoodRepository = $neighborhoodRepo;
    }

    /**
     * Display a listing of the Neighborhood.
     *
     * @param NeighborhoodDataTable $neighborhoodDataTable
     * @return Response
     */
    public function index(NeighborhoodDataTable $neighborhoodDataTable)
    {
        return $neighborhoodDataTable->render('neighborhoods.index');
    }

    /**
     * Show the form for creating a new Neighborhood.
     *
     * @return Response
     */
    public function create()
    {
        return view('neighborhoods.create');
    }

    /**
     * Store a newly created Neighborhood in storage.
     *
     * @param CreateNeighborhoodRequest $request
     *
     * @return Response
     */
    public function store(CreateNeighborhoodRequest $request)
    {
        $input = $request->all();

        $neighborhood = $this->neighborhoodRepository->create($input);

        Flash::success('Neighborhood saved successfully.');

        return redirect(route('neighborhoods.index'));
    }

    /**
     * Display the specified Neighborhood.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            Flash::error('Neighborhood not found');

            return redirect(route('neighborhoods.index'));
        }

        return view('neighborhoods.show')->with('neighborhood', $neighborhood);
    }

    /**
     * Show the form for editing the specified Neighborhood.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            Flash::error('Neighborhood not found');

            return redirect(route('neighborhoods.index'));
        }

        return view('neighborhoods.edit')->with('neighborhood', $neighborhood);
    }

    /**
     * Update the specified Neighborhood in storage.
     *
     * @param  int              $id
     * @param UpdateNeighborhoodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNeighborhoodRequest $request)
    {
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            Flash::error('Neighborhood not found');

            return redirect(route('neighborhoods.index'));
        }

        $neighborhood = $this->neighborhoodRepository->update($request->all(), $id);

        Flash::success('Neighborhood updated successfully.');

        return redirect(route('neighborhoods.index'));
    }

    /**
     * Remove the specified Neighborhood from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            Flash::error('Neighborhood not found');

            return redirect(route('neighborhoods.index'));
        }

        $this->neighborhoodRepository->delete($id);

        Flash::success('Neighborhood deleted successfully.');

        return redirect(route('neighborhoods.index'));
    }
}
