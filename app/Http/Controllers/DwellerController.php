<?php

namespace App\Http\Controllers;

use App\DataTables\DwellerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDwellerRequest;
use App\Http\Requests\UpdateDwellerRequest;
use App\Models\House;
use App\Repositories\DwellerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DwellerController extends AppBaseController
{
    /** @var  DwellerRepository */
    private $dwellerRepository;

    public function __construct(DwellerRepository $dwellerRepo)
    {
        $this->dwellerRepository = $dwellerRepo;
    }

    /**
     * Display a listing of the Dweller.
     *
     * @param DwellerDataTable $dwellerDataTable
     * @return Response
     */
    public function index(DwellerDataTable $dwellerDataTable)
    {
        return $dwellerDataTable->render('dwellers.index');
    }

    /**
     * Show the form for creating a new Dweller.
     *
     * @return Response
     */
    public function create()
    {
        $houses = House::orderBy('street')->pluck('street','id')->all();

        return view('dwellers.create',compact('houses'));
    }

    /**
     * Store a newly created Dweller in storage.
     *
     * @param CreateDwellerRequest $request
     *
     * @return Response
     */
    public function store(CreateDwellerRequest $request)
    {
        $input = $request->all();

        $dweller = $this->dwellerRepository->create($input);

        Flash::success('Dweller saved successfully.');

        return redirect(route('dwellers.index'));
    }

    /**
     * Display the specified Dweller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dweller = $this->dwellerRepository->find($id);

        if (empty($dweller)) {
            Flash::error('Dweller not found');

            return redirect(route('dwellers.index'));
        }

        return view('dwellers.show')->with('dweller', $dweller);
    }

    /**
     * Show the form for editing the specified Dweller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dweller = $this->dwellerRepository->find($id);

        $houses = House::orderBy('street')->pluck('street','id')->all();

        if (empty($dweller)) {
            Flash::error('Dweller not found');

            return redirect(route('dwellers.index'));
        }

        return view('dwellers.edit',compact('houses'))->with('dweller', $dweller);
    }

    /**
     * Update the specified Dweller in storage.
     *
     * @param  int              $id
     * @param UpdateDwellerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDwellerRequest $request)
    {
        $dweller = $this->dwellerRepository->find($id);

        if (empty($dweller)) {
            Flash::error('Dweller not found');

            return redirect(route('dwellers.index'));
        }

        $dweller = $this->dwellerRepository->update($request->all(), $id);

        Flash::success('Dweller updated successfully.');

        return redirect(route('dwellers.index'));
    }

    /**
     * Remove the specified Dweller from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dweller = $this->dwellerRepository->find($id);

        if (empty($dweller)) {
            Flash::error('Dweller not found');

            return redirect(route('dwellers.index'));
        }

        $this->dwellerRepository->delete($id);

        Flash::success('Dweller deleted successfully.');

        return redirect(route('dwellers.index'));
    }
}
