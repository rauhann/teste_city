<?php

namespace App\DataTables;

use App\Models\House;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Datatables;

class HouseDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     * @throws \Exception
     */
    public function dataTable()
    {
        $houses = House::all();

        foreach($houses as $house)
            $house['neighborhood_name'] = $house->neighborhood->name;

            return Datatables::of($houses)
                ->addColumn('action', 'houses.datatables_actions')
                ->rawColumns(['action']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'street'            => ['title' => 'Rua'],
            'number'            => ['title' => 'Numero'],
            'neighborhood_name' => ['title' => 'Bairro']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'housesdatatable_' . time();
    }
}
