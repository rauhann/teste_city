<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Profession
 * @package App\Models
 * @version June 13, 2019, 12:51 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection dwellers
 * @property string name
 * @property integer dweller_id
 */
class Profession extends Model
{
    use SoftDeletes;

    public $table = 'professions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'dweller_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'dweller_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'dweller_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function dwellers()
    {
        return $this->belongsTo(\App\Models\Dweller::class, 'dweller_id', 'id');
    }
}
