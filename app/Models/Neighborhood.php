<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Neighborhood
 * @package App\Models
 * @version June 13, 2019, 12:50 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection houses
 * @property string name
 */
class Neighborhood extends Model
{
    use SoftDeletes;

    public $table = 'neighborhoods';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function houses()
    {
        return $this->belongsToMany(\App\Models\House::class, 'neighborhood_id');
    }
}
