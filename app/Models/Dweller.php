<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Dweller
 * @package App\Models
 * @version June 13, 2019, 12:51 pm UTC
 *
 * @property \App\Models\House house
 * @property \App\Models\Profession profession
 * @property string name
 * @property integer age
 * @property integer house_id
 */
class Dweller extends Model
{
    use SoftDeletes;

    public $table = 'dwellers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'age',
        'house_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'age' => 'integer',
        'house_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'age' => 'required',
        'house_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function house()
    {
        return $this->belongsTo(\App\Models\House::class, 'house_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function profession()
    {
        return $this->hasMany(\App\Models\Profession::class, 'dweller_id');
    }
}
