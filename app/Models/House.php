<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class House
 * @package App\Models
 * @version June 13, 2019, 12:50 pm UTC
 *
 * @property \App\Models\Neighborhood neighborhood
 * @property \Illuminate\Database\Eloquent\Collection dwellers
 * @property string street
 * @property integer number
 * @property integer neighborhood_id
 */
class House extends Model
{
    use SoftDeletes;

    public $table = 'houses';
    

    protected $dates = ['deleted_at'];

    protected $appends = ['created_at2'];


    public $fillable = [
        'street',
        'number',
        'neighborhood_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'street' => 'string',
        'number' => 'integer',
        'neighborhood_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'street' => 'required',
        'number' => 'required',
        'neighborhood_id' => 'required'
    ];

    public function getCreatedAt2Attribute()
    {
        if(isset($this->attributes['created_at']))
        {
            return date('d/m/Y H:i', strtotime($this->attributes['created_at']));
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function neighborhood()
    {
        return $this->belongsTo(\App\Models\Neighborhood::class, 'neighborhood_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function dwellers()
    {
        return $this->hasMany(\App\Models\Dweller::class, 'house_id');
    }
}
