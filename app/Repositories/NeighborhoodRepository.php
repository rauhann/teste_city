<?php

namespace App\Repositories;

use App\Models\Neighborhood;
use App\Repositories\BaseRepository;

/**
 * Class NeighborhoodRepository
 * @package App\Repositories
 * @version June 13, 2019, 12:50 pm UTC
*/

class NeighborhoodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Neighborhood::class;
    }
}
