<?php

namespace App\Repositories;

use App\Models\House;
use App\Repositories\BaseRepository;

/**
 * Class HouseRepository
 * @package App\Repositories
 * @version June 13, 2019, 12:50 pm UTC
*/

class HouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'street',
        'number',
        'neighborhood_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return House::class;
    }
}
