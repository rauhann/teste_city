<?php

namespace App\Repositories;

use App\Models\Profession;
use App\Repositories\BaseRepository;

/**
 * Class ProfessionRepository
 * @package App\Repositories
 * @version June 13, 2019, 12:51 pm UTC
*/

class ProfessionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'dweller_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profession::class;
    }
}
