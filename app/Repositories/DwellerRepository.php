<?php

namespace App\Repositories;

use App\Models\Dweller;
use App\Repositories\BaseRepository;

/**
 * Class DwellerRepository
 * @package App\Repositories
 * @version June 13, 2019, 12:51 pm UTC
*/

class DwellerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'age',
        'house_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dweller::class;
    }
}
